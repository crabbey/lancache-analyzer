package main

import "fmt"
import "flag"
import "os"
import "path/filepath"
import "bufio"
import "time"
import "regexp"
import "runtime"
import "encoding/json"
import "github.com/fsnotify/fsnotify"
import "net/http"

var files []string
var Storage = DataStorage{}

func init() {
    var cachedir string
    var ListenString string
    var Debug bool
    var Watch bool
    var origUsage = flag.Usage;
    flag.Usage = func() {
        fmt.Fprintln(os.Stdout, "Catalogs your NginX cache files for easy diagnostics.")
        fmt.Fprintln(os.Stdout, "Once the catalog is complete, any HTTP request will return any files whose cache keys match the request, left matched")
        fmt.Fprintln(os.Stdout, "Example:")
        fmt.Fprintln(os.Stdout, "    curl 127.0.0.1:8080/test/cached/file")
        fmt.Fprintln(os.Stdout, "will return any on-disk file name where the cache key matches 'test/cached/file*'")
        fmt.Fprintln(os.Stdout, "Including:")
        fmt.Fprintln(os.Stdout, "    test/cached/file/10")
        fmt.Fprintln(os.Stdout, "    test/cached/filename")
        fmt.Fprintln(os.Stdout, "    test/cached/file/but/deeper")
        fmt.Fprintln(os.Stdout, "")
        fmt.Fprintln(os.Stdout, "You may append ?json to produce a JSON formatted list that tells you both the on-disk file and the cache key")
        origUsage()
    }
    flag.StringVar(&cachedir, "d", ".", "Directory containing your cache files")
    flag.StringVar(&ListenString, "l", "0.0.0.0:8080", "Port to listen on")
    flag.BoolVar(&Watch, "w", false, "Watch for fsnotify events - BETA")
    flag.BoolVar(&Debug, "v", false, "Debug")
    flag.Parse()
    tmp, _ := filepath.Abs(cachedir)
    
    Storage.Cachedir     = &tmp
    Storage.ListenString = &ListenString
    Storage.Debug        = &Debug
    Storage.Watch        = &Watch
    Storage.Lock         = false
    Storage.Foundfile    = make(chan string);
    Storage.Entries      = map[string]*Entry{}
    Storage.Control      = make(chan string);
    Storage.NewEntries   = make(chan *Entry);
}

func main() {
    fmt.Println("Searching in directory " + *Storage.Cachedir + " for nginx cache files")
    for i := 1; i < runtime.NumCPU(); i++ {
        // one of these per CPU for maximum oof
        go fileReader()
    }
    go serve()
    go debug()
    go fileStorer()
    initMonitor()
    go monitor()
    listFiles()
    select {}
}

func fileStorer() {
    for {
        if (Storage.Lock) {
            time.Sleep(100 * time.Millisecond)
            continue
        }
        entry := <- Storage.NewEntries
        Storage.Entries[*entry.Filename] = entry;        
    }
}

func initMonitor() {
    watcher, err := fsnotify.NewWatcher()
    checkErr(err);
    Storage.Watcher = watcher
}

func monitor() {
    defer Storage.Watcher.Close()
    for {
        select {
        case event, ok := <-Storage.Watcher.Events:
            if !ok {
                return
            }
            // fmt.Println("event:", event)
            if event.Op&fsnotify.Write == fsnotify.Write {
                // fmt.Println("modified file:", event.Name)
                Storage.Foundfile <- event.Name
            }
        case err, ok := <-Storage.Watcher.Errors:
            fmt.Println("error:", err)
            if !ok {
                return
            }
        }
    }
}

func fileReader() {
    defer func() {
        _ = recover();
    }()
    for {
        filename := <- Storage.Foundfile
        f, err := os.Open(filename)

        checkErr(err)
        r4 := bufio.NewReader(f)
        var bytes = []byte("KEY: ");
        for _, byte := range bytes {
            r4.ReadSlice(byte);
        }
        cachekey, _, err := r4.ReadLine()
        // checkErr(err)
        if (cachekey == nil) {
            continue;
        }
        strcachekey := string(cachekey)
        newEntry := Entry {
            Filename: &filename,
            Cachekey: &strcachekey,
        }
        Storage.NewEntries <- &newEntry;
        f.Close()
    }
}

func listFiles() {
    var err error
    if (*Storage.Watch) {
        err = Storage.Watcher.Add(*Storage.Cachedir)
        checkErr(err)        
    }
    err = filepath.Walk(*Storage.Cachedir, func(path string, info os.FileInfo, err error) error {
        if (info.IsDir()) {
            if (path == ".") {
                return nil;
            }
            if (*Storage.Watch) {
                err = Storage.Watcher.Add(*Storage.Cachedir)
                checkErr(err)        
            }
            return nil
        }
        Storage.Foundfile <- path
        return nil
    })
    if err != nil {
        panic(err)
    }
}

func checkErr(e error) {
    if e != nil {
        panic(e)
    }
}

func search(path string) ([]*Entry) {
    sanitise := regexp.MustCompile(`^\/`)
    path = sanitise.ReplaceAllLiteralString(path, "")
    sanitise = regexp.MustCompile(`\/`)
    path = sanitise.ReplaceAllLiteralString(path, "\\/")
    exp, _ := regexp.Compile("^"+path)
    var resp []*Entry
    Storage.Lock = true
    // Sleep to ensure we have the lock and the map won't be modified
    time.Sleep(100 * time.Millisecond)
    for _, entry := range Storage.Entries {
        out := exp.FindString(*entry.Cachekey)
        if out != "" {
            resp = append(resp, entry)
        }
    }
    Storage.Lock = false
    return resp
}

func serve() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        mode := "raw"
        query := r.URL.Query()
        for k, _ := range query {
            if k == "json" {
                mode = "json"
            }
        }
        entries := search(r.URL.Path)
        if mode == "raw" {
            for _, entry := range entries {
                fmt.Fprintf(w, "%s\n", *entry.Filename);
            }
        } else if mode == "json" {
            json, err := json.Marshal(entries)
            if err != nil {
                fmt.Println(err)
            }
            fmt.Fprint(w, string(json))
        }
    })

    fmt.Println("Listening on", *Storage.ListenString, "for HTTP requests");
    fmt.Println(http.ListenAndServe(*Storage.ListenString, nil))
}

func debug() {
    _ = time.Second
    for {
        select {
               case recv := <- Storage.Control:
                if (recv == "exit") {
                    os.Exit(1);
                }
            case <-time.After(10 * time.Second):
                if *Storage.Debug {
                    fmt.Printf("Current items in cache: %d\n", len(Storage.Entries))
                }
        }
    }
}