package main

import "github.com/fsnotify/fsnotify"

type Entry struct {
	Filename *string
	Cachekey *string
}

type DataStorage struct {
	Entries      map[string]*Entry
	Cachedir     *string
	Debug        *bool
	Watch        *bool
	Lock         bool
	ListenString *string
	Foundfile    chan string
	Control      chan string
	Watcher      *fsnotify.Watcher
	NewEntries   chan *Entry
}